# HTML講座

### 8/2(木)
- 自己紹介
  - 人間関係を創る

- 歴史（mosaic, netscape...) W3C
- htmlとは
- cssとは
- Webサーバとは

- 環境構築
  - atom ダウンロード
  - emmet プラグイン 導入

- atomエディタの使い方
  - atom のいろいろなプラグイン
    - [愛用しているプラグイン](https://qiita.com/Atelier-Mirai/items/7ba376d7eda116a663b5)
  - atom のカラーテーマ

- フットサルサイトを作ってみよう(とってもシンプル版)
  - !doctype html
    - htmlの構成
  - [htmlタグ一覧](http://www.htmq.com/html5/)
    p, a などなど
  - [cssプロパティ一覧](http://www.htmq.com/style/)
    color, background などなど

- [HTMLの基本](http://www.htmq.com/htmlkihon/)
- [CSSの基本](http://www.htmq.com/csskihon/)

- 参考書籍
  - [HTML5/CSS3モダンコーディング](https://www.amazon.co.jp/HTML5-CSS3モダンコーディング-フロントエンドエンジニアが教える3つの本格レイアウト-スタンダード・グリッド・シングルページレイアウトの作り方-Engineer’s/dp/4798141577/ref=sr_1_1?ie=UTF8&qid=1533012456&sr=8-1&keywords=html5%2Fcss3モダンコーディング)
  - [はじめてのCSS設計](https://www.amazon.co.jp/はじめてのCSS設計-フロントエンドエンジニアが教えるメンテナブルなCSS設計手法-WEB-Engineer’s-Books/dp/4798143154/ref=sr_1_1?s=books&ie=UTF8&qid=1533012500&sr=1-1&keywords=はじめてのcss設計)

- 参考サイト
  - [ドットインストール](https://dotinstall.com/lessons) 初心者向けの3分動画
  - [progate](https://prog-8.com) 初心者向けのプログラミング学習
  - [HTMLクイックリファレンス](http://www.htmq.com/html5/index.shtml)
  - [和色大辞典](http://www.colordic.org/w/) 美しい日本の伝統色の紹介
  - [e-typing](https://dotinstall.com/lessons) タイピング練習サイト

- 有益な情報・質問
  - [Qiita](https://qiita.com)
  - [stack overflow](https://ja.stackoverflow.com)

- Web上で開発
  - [CodePen](https://www.codepen.io)
  - [paiza](https://paiza.io/ja)
  - [Cloud9](https://c9.io/login)

- Mac capslock -> ctrl

### 8/9(木)
- フットサルサイトのサブセット（トップページ＆他のページ）

- ブラウザの種類
  - Chrome/Safariを中心に。
- ブラウザの開発者ツールの使い方
  - スタイルの確認などができ、便利

- css 詳細度
- css でボタンを作る（基本 / 便利なサイト）

- CSS対応状況
  - [Can I use?](https://caniuse.com)

### 8/16(木)
- お盆休み

### 8/23(木)
- レスポンシブデザイン
  - メディアクエリ
- フットサルサイト レスポンシブ対応

- flexbox
- ラスタ画像png, jpg / ベクタ画像svg
- Webフォント

- Javascript(JS)とは
  - 簡単にJavascript練習
- jQueryとは
- 写真や動画の表示
- lightboxとは
  - tosrus/Colorbox/Featherlight/Magnific Popup/lightGallery(有料)

### 8/30(木)
- フットサルサイト完成

- 落ち穂拾い
  - シングルクラスでのcss指定、マルチクラスでのcss指定
  - css スプライト
  - 整形(html/css)

- 未來へ
  - Bootstrap / Foundation
  - Sass(SCSS) / Compass
  - Slim
  - git
  - Affinity Photo / Affinity Designer / Affinity Publisher
  - WordPress / Ruby on Rails の違い
