### 8/30(木)

- フットサルサイト完成

- 落ち穂拾い
  - シングルクラスでのcss指定、マルチクラスでのcss指定
  - css スプライト
  - 整形(html/css)

- 未來へ
  - Bootstrap / Foundation
  - Sass(SCSS) / Compass
  - Slim
  - git
  - Affinity Photo / Affinity Designer / Affinity Publisher
  - WordPress / Ruby on Rails の違い
