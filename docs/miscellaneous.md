
[grid systemとは？](http://websae.net/twitter-bootstrap-grid-system-21060224/)
[レスポンシブ対応のグリッド システムを実装する時に絶対覚えておきたいテクニックを詳しく解説](https://coliss.com/articles/build-websites/operation/css/how-to-responsive-grid.html) HTMLグリッドシステム / CSS グリッドシステム

[960 Grid System](https://960.gs)
[Responsive Grid System](https://www.bigdropinc.com/blog/responsive-gs/) 12, 16 & 24 column

レイアウトの歴史

| レイアウト名                  | 特徴                                                   |
| --------------------------- | ----------------------------------------------------- |
| レイアウトなし                | 一番初期のWebサイト                                       |
| テーブルレイアウト             | 最初に登場したレイアウト。今は使ってはいけない。                 |
| float (clearfix) レイアウト | 使っているサイトは多いがこれから使わなくてもいいかな。           |
| flexbox                     | レイアウトが自由にできる(一次元)ので、今からはこれかな。               |
| CSS Grid Layout             | 二次元レイアウトを簡単に実現。今後のために学んでおくとよいかな。 |


[CSS Grid Layoutを極める](https://qiita.com/kura07/items/e633b35e33e43240d363)
